package com.epam.sort;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Random;

/**
 * Created by Aleksandr_Kataev
 * On: 31-Oct-16
 */
public class DataGenerator {
    public static void main(String[] args) throws Exception {
        final Random rand = new Random();

        try (final PrintWriter bw = new PrintWriter(new BufferedWriter(new FileWriter("data.txt")))) {
            int n = 1_000_000;
            bw.println(n);
            for (int i = 0; i < n; i++) {
                bw.print(rand.nextInt());
                bw.print(' ');
            }
            bw.flush();
        }
    }
}
