package com.epam.sort;

import java.io.File;
import java.util.Scanner;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;

/**
 * Created by Aleksandr_Kataev
 * On: 31-Oct-16
 */
public class MergeSortMain {

    public static void main(String[] args) throws Exception {
        final Scanner scan = new Scanner(new File("data.txt"));
        int n = scan.nextInt();
        int[] input = new int[n];
        for (int i = 0; i < n; i++) {
            input[i] = scan.nextInt();
        }
        System.out.println(n);
        //int[] input = {2, 4, 2, 9, 7};
        long start = System.currentTimeMillis();
        // new MergeSort(input).sort();

        ForkJoinPool forkJoinPool = ForkJoinPool.commonPool();
        ForkJoinTask<Void> task = forkJoinPool.submit(new MergeSort(input));
        task.join();
        // Arrays.parallelSort(input);

        long end = System.currentTimeMillis();
        long elapsed = end - start;
        System.out.println("Elapsed: " + elapsed);
        //System.out.println(Arrays.toString(input));
        for (int i = 0; i < input.length - 1; i++) {
            assert input[i + 1] >= input[i];
        }
    }


    static class MergeSort extends RecursiveTask<Void> {

        private final int[] arr;
        private final int[] temp;
        private final int left;
        private final int right;

        public MergeSort(int[] arr) {
            this(arr, new int[arr.length], 0, arr.length);
        }

        public MergeSort(int[] arr, int[] temp, int l, int r) {
            this.arr = arr;
            this.temp = temp;
            this.left = l;
            this.right = r;
        }

        public void sort() {
            sort(0, arr.length);
        }

        private void sort(int l, int r) {
            if (r <= l + 1) {
                return;
            }
            int mid = (r + l) >>> 1;
            sort(l, mid);
            sort(mid, r);
            merge(l, mid, r);
        }

        private void merge(int l, int mid, int r) {
            // arr[i..mid - 1] + arr[j..r - 1] -> temp[l..r - 1] -> arr[l..r-1]
            int i = l;
            int j = mid;
            for (int k = l; k < r; k++) {
                if (j >= r || (i < mid && arr[i] < arr[j])) {
                    temp[k] = arr[i++];
                } else {
                    temp[k] = arr[j++];
                }
            }
            System.arraycopy(temp, l, arr, l, r - l);
        }

        private void parallelSort() {
            if (right <= left + 1) {
                return;
            }
            int mid = (right + left) >>> 1;
            final ForkJoinTask<Void> leftSort = new MergeSort(arr, temp, left, mid);
            final ForkJoinTask<Void> rightSort = new MergeSort(arr, temp, mid, right);
            invokeAll(leftSort, rightSort);

            rightSort.join();
            leftSort.join();

            merge(left, mid, right);
        }

        @Override
        public Void compute() {
            parallelSort();
            return null;
        }
    }
}
