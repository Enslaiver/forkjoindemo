package com.epam;

import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) throws MyThrowable {
        System.out.println("Hello");

        List l = Arrays.asList(new int[]{1, 2, 3});
        System.out.println(l.size());
        for (Object o : l) {
            System.out.println(o);
        }

        try {
            // throw new MyThrowable();
        } catch (Exception e) {
            System.out.println("Exception");
        } catch (Error error) {
            System.out.println("Error");
        }
    }

    static class MyThrowable extends Throwable {

    }

}
